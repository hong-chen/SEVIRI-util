# Purpose:
#   Read SEVIRI data
#
# by Hong Chen (me@hongchen.cz)
#
# Tested on macOS v10.12.6 with
#   - Python v3.6.0

import os
import sys
import glob
import datetime
import numpy as np
import matplotlib.path as mpl_path
import cartopy.crs as ccrs
import ftplib
from pyhdf.SD import SD, SDC
from scipy import interpolate
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator
from matplotlib import rcParams
import matplotlib.patches as patches
import cartopy.crs as ccrs

class SEVIRI_L2:

    """
    input:
        namePattern: e.g. SEV_06-CLD-L2_2016-08-02T05-15-00_V2-00.hdf
        vnameExtra: default is '', can be "16" etc.
        fdir: the data directory

    output:
        a class object that contains:
            1. self.lon
            2. self.lat
            3. self.ctp: cloud thermodynamic phase
            4. self.cot
            5. self.cer
            6. self.cot_pcl
            7. self.cer_pcl
            6. self.cot_unc
            7. self.cer_unc
            8. self.COLLOCATE(lon_in, lat_in):
                8.1. self.lon_domain
                8.2. self.lat_domain
                8.3. self.cot_domain
                8.4. self.cer_domain
                8.5. self.cot_pcl_domain
                8.6. self.cer_pcl_domain
                8.5. self.cot_unc_domain
                8.6. self.cer_unc_domain
                8.7. self.ctp_domain
                8.8 . self.lon_collo
                8.9 . self.lat_collo
                8.10. self.cot_collo
                8.11. self.cer_collo
                8.12. self.cot_pcl_collo
                8.13. self.cer_pcl_collo
                8.12. self.cot_unc_collo
                8.13. self.cer_unc_collo
                8.14. self.cot_collo_all
                8.15. self.cer_collo_all
                8.16. self.ctp_collo
    """

    def __init__(self, fname, copFlag='16'):

        fname  = os.path.abspath(fname)
        if not os.path.isfile(fname):
            exit('Error [SEVIRI_L2]: can not find file \'%s\'.' % (fname))

        self.fname = fname

        fname_cld = fname
        f_cld = SD(fname_cld, SDC.READ)

        lon = f_cld.select('Longitude')[:]
        lon[lon<0.0] += 360.0
        self.lon = lon
        self.lat = f_cld.select('Latitude')[:]

        vname_ctp = 'Cloud_Phase_Optical_Properties'
        self.ctp  = np.int_(f_cld.select(vname_ctp)[:] * f_cld.select(vname_ctp).attributes()['scale_factor'])

        vname_mask = 'Cloud_Mask'
        self.mask  = np.int_(f_cld.select(vname_ctp)[:] * f_cld.select(vname_ctp).attributes()['scale_factor'])

        if copFlag == None:
            vname_cot = 'Cloud_Optical_Thickness'
            self.cot = f_cld.select(vname_cot)[:] * f_cld.select(vname_cot).attributes()['scale_factor']
            vname_cer = 'Cloud_Effective_Radius'
            self.cer = f_cld.select(vname_cer)[:] * f_cld.select(vname_cer).attributes()['scale_factor']

            vname_cot     = 'Cloud_Optical_Thickness_PCL'
            self.cot_pcl  = f_cld.select(vname_cot)[:] * f_cld.select(vname_cot).attributes()['scale_factor']
            vname_cer     = 'Cloud_Effective_Radius_PCL'
            self.cer_pcl  = f_cld.select(vname_cer)[:] * f_cld.select(vname_cer).attributes()['scale_factor']

            vname_cot     = 'Cloud_Optical_Thickness_Uncertainty'
            self.cot_unc  = f_cld.select(vname_cot)[:] * f_cld.select(vname_cot).attributes()['scale_factor']
            vname_cer     = 'Cloud_Effective_Radius_Uncertainty'
            self.cer_unc  = f_cld.select(vname_cer)[:] * f_cld.select(vname_cer).attributes()['scale_factor']

        else:
            vname_cot = 'Cloud_Optical_Thickness_%s' % (copFlag)
            self.cot = f_cld.select(vname_cot)[:] * f_cld.select(vname_cot).attributes()['scale_factor']
            vname_cer = 'Cloud_Effective_Radius_%s' % (copFlag)
            self.cer = f_cld.select(vname_cer)[:] * f_cld.select(vname_cer).attributes()['scale_factor']

            vname_cot     = 'Cloud_Optical_Thickness_%s_PCL' % (copFlag)
            self.cot_pcl  = f_cld.select(vname_cot)[:] * f_cld.select(vname_cot).attributes()['scale_factor']
            vname_cer     = 'Cloud_Effective_Radius_%s_PCL' % (copFlag)
            self.cer_pcl  = f_cld.select(vname_cer)[:] * f_cld.select(vname_cer).attributes()['scale_factor']

            vname_cot     = 'Cloud_Optical_Thickness_Uncertainty_%s' % (copFlag)
            self.cot_unc  = f_cld.select(vname_cot)[:] * f_cld.select(vname_cot).attributes()['scale_factor']
            vname_cer     = 'Cloud_Effective_Radius_Uncertainty_%s' % (copFlag)
            self.cer_unc  = f_cld.select(vname_cer)[:] * f_cld.select(vname_cer).attributes()['scale_factor']

        f_cld.end()


    def COLLOCATE(self, lon_in, lat_in, tmhr_in=None):

        lon_in[lon_in<0.0] += 360.0
        logic = (self.lon>(lon_in.min()-0.2)) & (self.lon<(lon_in.max()+0.2)) & \
                (self.lat>(lat_in.min()-0.2)) & (self.lat<(lat_in.max()+0.2))

        self.lon_domain     = self.lon[logic].ravel()
        self.lat_domain     = self.lat[logic].ravel()
        self.cot_domain     = self.cot[logic].ravel()
        self.cer_domain     = self.cer[logic].ravel()
        self.cot_pcl_domain = self.cot_pcl[logic].ravel()
        self.cer_pcl_domain = self.cer_pcl[logic].ravel()
        self.cot_unc_domain = self.cot_unc[logic].ravel()
        self.cer_unc_domain = self.cer_unc[logic].ravel()
        self.ctp_domain     = self.ctp[logic].ravel()

        self.cot_domain_all = self.cot_domain.copy()
        self.cer_domain_all = self.cer_domain.copy()
        logic = ((self.cot_domain<0.0)&(self.cot_pcl_domain>0.0)) & ((self.cer_domain<0.0)&(self.cer_pcl_domain>0.0))
        self.cot_domain_all[logic] = self.cot_pcl_domain[logic]
        self.cer_domain_all[logic] = self.cer_pcl_domain[logic]

        points = np.array(list(zip(self.lon_domain, self.lat_domain)))

        self.lon_collo  = lon_in
        self.lat_collo  = lat_in
        if tmhr_in is not None:
            self.tmhr_collo = tmhr_in

        self.cot_collo     = interpolate.griddata(points, self.cot_domain    , (lon_in, lat_in), method='nearest')
        self.cer_collo     = interpolate.griddata(points, self.cer_domain    , (lon_in, lat_in), method='nearest')
        self.cot_pcl_collo = interpolate.griddata(points, self.cot_pcl_domain, (lon_in, lat_in), method='nearest')
        self.cer_pcl_collo = interpolate.griddata(points, self.cer_pcl_domain, (lon_in, lat_in), method='nearest')
        self.cot_unc_collo = interpolate.griddata(points, self.cot_unc_domain, (lon_in, lat_in), method='nearest')
        self.cer_unc_collo = interpolate.griddata(points, self.cer_unc_domain, (lon_in, lat_in), method='nearest')
        self.pcl_logic = ((self.cot_collo<0.0)&(self.cot_pcl_collo>0.0)) & ((self.cer_collo<0.0)&(self.cer_pcl_collo>0.0))

        self.cot_collo_all = interpolate.griddata(points, self.cot_domain_all, (lon_in, lat_in), method='nearest')
        self.cer_collo_all = interpolate.griddata(points, self.cer_domain_all, (lon_in, lat_in), method='nearest')

        self.ctp_collo      = interpolate.griddata(points, self.ctp_domain    , (lon_in, lat_in), method='nearest')





def EARTH_VIEW(seviri):

    """
    Purpose:
        Plot SEVIRI data on the map (globe).

    input:
        seviri: SEVIRI_L2 object

    """

    rcParams['font.size'] = 8.0

    center_lon = 0.0
    center_lat = 0.0

    proj = ccrs.Orthographic(central_longitude=center_lon, central_latitude=center_lat)

    ax = plt.axes(projection=proj)
    ax.set_global()
    ax.stock_img()
    ax.coastlines(color='gray', lw=0.2)
    title = os.path.basename(seviri.fname)
    ax.set_title(title, fontsize=8)

    cs = ax.scatter(seviri.lon, seviri.lat, transform=ccrs.Geodetic(), s=0.01, c=seviri.cot, cmap='jet', edgecolor='none', vmin=0.0, vmax=50.0)
    plt.colorbar(cs, shrink=0.6)
    plt.savefig('%s.png' % '.'.join(title.split('.')[:-1]))
    plt.show()
    # plt.close()
    # ---------------------------------------------------------------------






def GTIME(fname, doy=True):

    filename = os.path.basename(fname)
    words    = filename.split('_')
    # e.g. 2016-09-20T11-30-00
    dtime    = datetime.datetime.strptime(words[2], '%Y-%m-%dT%H-%M-%S')
    if doy:
        return (dtime-datetime.datetime(dtime.year-1, 12, 31)).total_seconds() / 86400.0
    else:
        return dtime






if __name__ == '__main__':

    fname = 'data/SEV_06-CLD-L2_2016-09-20T11-30-00_V2-00.hdf'
    sat = SEVIRI_L2(fname)
    EARTH_VIEW(sat)

    # dtime = GTIME(fname, doy=True)
    # print(dtime)
